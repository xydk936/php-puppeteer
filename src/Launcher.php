<?php
/**
 * @autohr by <xydk936>.
 * Date: 2019-08-10
 * Time: 15:24
 */

use Swoole\Process;
const DEFAULT_ARGS = [
    '--disable-background-networking',
    '--enable-features=NetworkService,NetworkServiceInProcess',
    '--disable-background-timer-throttling',
    '--disable-backgrounding-occluded-windows',
    '--disable-breakpad',
    '--disable-client-side-phishing-detection',
    '--disable-component-extensions-with-background-pages',
    '--disable-default-apps',
    '--disable-dev-shm-usage',
    '--disable-extensions',
    // TODO: Support OOOPIF. @see https://github.com/GoogleChrome/puppeteer/issues/2548
    // BlinkGenPropertyTrees disabled due to crbug.com/937609
    '--disable-features=site-per-process,TranslateUI,BlinkGenPropertyTrees',
    '--disable-hang-monitor',
    '--disable-ipc-flooding-protection',
    '--disable-popup-blocking',
    '--disable-prompt-on-repost',
    '--disable-renderer-backgrounding',
    '--disable-sync',
    '--force-color-profile=srgb',
    '--metrics-recording-only',
    '--no-first-run',
    '--enable-automation',
    '--password-store=basic',
    '--use-mock-keychain',
];

class Launcher
{
    private $projectRoot;
    private $preferredRevision;
    private $isPuppeteerCore;
    private $chromeProcess;

    public function __construct(string $projectRoot, int $preferredRevision, bool $isPuppeteerCore)
    {
        $this->projectRoot = $projectRoot;
        $this->preferredRevision = $preferredRevision;
        $this->isPuppeteerCore = $isPuppeteerCore;
    }

    public function launch(array $options = []): ?Browser
    {
        $defaultOpt = [
            'ignoreDefaultArgs' => false,
            'args' => [],
            'dumpio' => false,
            'executablePath' => null,
            'pipe' => false,
            'env' => getenv(),
            'handleSIGINT' => true,
            'handleSIGTERM' => true,
            'handleSIGHUP' => true,
            'ignoreHTTPSErrors' => false,
            'defaultViewport' => ['width' => 800, 'height' => 600],
            'slowMo' => 0,
            'timeout' => 30000
        ];
        $options = $options + $defaultOpt; //defaultOpt 不会覆盖options

        $chromeArguments = [];
        if (!$options['ignoreDefaultArgs']) {
            $chromeArguments = $this->defaultArgs($options);
        } elseif (is_array($options['ignoreDefaultArgs'])) {
            $arr = $this->defaultArgs($options);
            $chromeArguments = array_filter($arr, function ($val) use ($options) {
                return in_array($val, $options['ignoreDefaultArgs']);
            });
        } else {
            $chromeArguments = $options['args'];
        }
        if (!array_some($chromeArguments, function ($val) {
            return startsWith($val, '--remote-debugging-');
        })) {
            $chromeArguments[] = $options['pipe'] ? '--remote-debugging-pipe' : '--remote-debugging-port=0';
        }

        if (!array_some($chromeArguments, function ($val) {
            return startsWith($val, '--user-data-dir');
        })) {
            //TODO 创建一个临时文件夹下的目录
//            $temporaryUserDataDir = tempnam(sys_get_temp_dir(), 'puppeteer_dev_profile-');
//            $chromeArguments[] = "--user-data-dir=${temporaryUserDataDir}";
        }

        $this->chromeProcess = new swoole_process(function (swoole_process $childProcess) use ($options, $chromeArguments) {
            print_r($chromeArguments);
            var_dump($childProcess->exec($options['executablePath'], $chromeArguments));
            var_dump($childProcess->read());
        });

        $this->chromeProcess->start();

        var_dump($this->chromeProcess->pid);
        Process::signal(SIGTERM, [$this, 'killChrome']);
        Process::signal(SIGCHLD, function ($sig) {
            //必须为false，非阻塞模式
            while ($ret = Process::wait(false)) {
                echo "PID={$ret['pid']}\n";
            }
        });

        return null;
    }

    private function defaultArgs($options = []): array
    {
        $defaultOpt = [
            'devtools' => false,
            'args' => [],
            'userDataDir' => null
        ];

        $defaultOpt['headless'] = array_key_exists('devtools', $options) ? !$options['devtools'] : true;

        $options = $options + $defaultOpt; //defaultOpt 不会覆盖options
        $chromeArguments = DEFAULT_ARGS;
        if ($options['userDataDir']) {
            $chromeArguments[] = "--user-data-dir=${options['userDataDir']}";
        }
        if ($options['devtools']) {
            $chromeArguments[] = '--auto-open-devtools-for-tabs';
        }
        if ($options['headless']) {
            array_push($chromeArguments,
                '--headless',
                '--hide-scrollbars',
                '--mute-audio');
        }
        if (array_every($chromeArguments, function ($arg) {
            return startsWith($arg, '-');
        })) {
            //有不合格的参数
            $chromeArguments[] = 'about:blank';
        }
        $chromeArguments = $chromeArguments + $options['args'];
        return $chromeArguments;
    }

    public function killChrome()
    {
        $this->chromeProcess->exit(0);
    }
}