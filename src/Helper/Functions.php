<?php
/**
 * @autohr by <xydk936>.
 * Date: 2019-08-29
 * Time: 14:23
 */

/**
 * @param array $input
 * @param callable $cb
 * @return bool
 */
function array_every(array $input, callable $cb): bool
{
    foreach ($input as $key => $value) {
        $result = $cb($value, $key, $input);
        if (!$result) {
            return false;
        }
    }
    return true;
}


function array_some(array $input, callable $cb): bool
{
    foreach ($input as $key => $value) {
        $result = $cb($value, $key, $input);
        if ($result) {
            return true;
        }
    }
    return false;
}

function startsWith(string $str, string $searchvalue, int $start = 0): bool
{
    return substr($str, $start, strlen($searchvalue)) == $searchvalue;
}