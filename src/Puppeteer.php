<?php
/**
 * @autohr by <xydk936>.
 * Date: 2019-08-13
 * Time: 17:23
 */


class Puppeteer
{
    private $projectRoot;
    private $launcher;


    public function __construct($projectRoot, $preferredRevision, $isPuppeteerCore)
    {
        $this->projectRoot = $projectRoot;
        $this->launcher = new Launcher($projectRoot, $preferredRevision, $isPuppeteerCore);
    }

    public function launch($options = []): ?Browser
    {
        return $this->launcher->launch($options);
    }

}